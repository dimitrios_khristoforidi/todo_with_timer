import * as React from "react";
import TasksListPage from "./containers/TasksListPage";
import "./index.css"

export default function App() {
    return (
        <TasksListPage/>
    )
}