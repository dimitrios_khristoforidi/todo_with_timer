import * as React from "react";
import {useEffect} from "react";
import "./index.css"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPause, faPlay, faTimes} from "@fortawesome/free-solid-svg-icons";
import {RootState} from "../../store/rootReducer";
import {connect} from "react-redux";
import {ITask} from "../../store/tasks/tasks.types";
import {deleteTask, switchTask, updateTask} from "../../store/tasks/actions";

export interface ITaskCard {
    data: ITask
    deleteTask: (id: number) => void
    switchTask: (id: number) => void
    updateTask: (id: number) => void
}

const TaskCard = ({data, deleteTask, switchTask, updateTask}: ITaskCard) => {
    // @ts-ignore
    useEffect(() => {
        if (!data.isPaused) {
            const interval = setInterval(() => {
                updateTask(data.id)
            }, 1000)
            return () => clearInterval(interval);
        }
    });

    const getTimer = (date: number): string => {
        let diffDays = Math.floor(date / (1000 * 60 * 60 * 24))
        let diffHrs = Math.floor((date % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        let diffMin = Math.floor((date % (1000 * 60 * 60)) / (1000 * 60));
        let diffSec = Math.floor((date % (1000 * 60)) / 1000);

        return ((diffDays ? (diffDays + "д ") : "") +
            (diffHrs ? (diffHrs + ":") : "00:") +
            (diffMin ? (diffMin + ":") : "00:") +
            (diffSec || "0"))
    }

    return (
        <div className="task-card">
            <div className="task-card__info">
                <h3>{data.text}</h3>
                <p>{getTimer(data.timestamp)}</p>
            </div>
            <div className="task-card__buttons">
                {
                    data.isPaused
                        ? <FontAwesomeIcon color="#5264AE" icon={faPlay}
                                           onClick={() => switchTask(data.id)}/>
                        : <FontAwesomeIcon color="#5264AE" icon={faPause}
                                           onClick={() => switchTask(data.id)}/>
                }
                <FontAwesomeIcon color="#5264AE" icon={faTimes} onClick={() => deleteTask(data.id)}/>
            </div>
        </div>
    )
}

const mapStateToProps = (state: RootState) => ({
    taskList: state.tasks.tasksList
})

const mapDispatchToProps = {
    deleteTask,
    switchTask,
    updateTask
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskCard)

