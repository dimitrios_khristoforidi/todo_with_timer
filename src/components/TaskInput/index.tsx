import * as React from "react";
import {useState} from "react";
import "./index.css"
import {connect} from "react-redux";
import {createTask} from "../../store/tasks/actions";

export interface ITasksInput {
    createTask: (text: string) => void
}

const TasksInput = ({createTask}: ITasksInput) => {
    const [newTaskText, setNewTaskText] = useState<string>("")

    const handleInput = (e: React.FormEvent<HTMLInputElement>) => {
        setNewTaskText(e.currentTarget.value)
    }

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        createTask(newTaskText)
        setNewTaskText("")
    }

    return (
        <form className="task-form" onSubmit={handleSubmit}>
            <div className="input-container">
                <input required type="text" className="input" value={newTaskText} onChange={handleInput}/>
                <span className="input-span">Task</span>
            </div>
            <button type="submit" className="button">ADD</button>
        </form>
    )
}

const mapDispatchToProps = {
    createTask
}

export default connect(null, mapDispatchToProps)(TasksInput)
