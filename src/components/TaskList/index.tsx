import * as React from "react";
import TaskCard from "../TaskCard";
import "./index.css"
import {connect} from "react-redux";
import {RootState} from "../../store/rootReducer";
import {ITask} from "../../store/tasks/tasks.types";

export interface ITasksList {
    tasksList?: ITask[]
}

const TasksList = ({tasksList}: ITasksList) => {
    return (
        <div className="tasks-list">
            {
                tasksList && tasksList.map((item, id) => (
                    <TaskCard key={id} data={item}/>
                ))
            }
        </div>
    )
}
const mapStateToProps = (state: RootState) => ({
    tasksList: state.tasks.tasksList
})

export default connect(mapStateToProps, null)(TasksList)
