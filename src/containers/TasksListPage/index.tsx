import * as React from "react";
import TasksInput from "../../components/TaskInput";
import TasksList from "../../components/TaskList";

export default function TasksListPage() {
    return (
        <div className="page-wrapper">
            <TasksInput/>
            <TasksList/>
        </div>
    )
}