import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from "./App";
import {Provider} from "react-redux";
import configureStore from "./store/configureStore";
import {PersistGate} from "redux-persist/integration/react";

const {store, persistor} = configureStore()

ReactDOM.render(
    <Provider store={store}>
        <PersistGate persistor={persistor} loading={null}>
            <App/>
        </PersistGate>
    </Provider>
    ,
    document.getElementById('root')
);