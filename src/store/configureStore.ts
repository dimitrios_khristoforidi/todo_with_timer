import {compose, createStore} from 'redux'
import {persistStore, persistReducer} from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import rootReducer from './rootReducer'

const persistConfig = {
    key: 'todo_timer',
    storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export default function configureStore() {
    // @ts-ignore
    const store = createStore(persistedReducer, compose(window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()))
    const persistor = persistStore(store)
    return {store, persistor}
}