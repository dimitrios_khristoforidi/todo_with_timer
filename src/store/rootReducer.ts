import {tasksReducer} from "./tasks/tasksReducer";
import {combineReducers} from "redux";

const rootReducer = combineReducers({
    tasks: tasksReducer
})

export default rootReducer
export type RootState = ReturnType<typeof rootReducer>
