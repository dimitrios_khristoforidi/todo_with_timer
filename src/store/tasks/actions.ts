import {CREATE_TASK, DELETE_TASK, SWITCH_TASK, TaskAction, UPDATE_TASK} from "./tasks.types";

export const createTask = (text: string): TaskAction => ({
    type: CREATE_TASK,
    payload: text
})

export const deleteTask = (id: number): TaskAction => ({
    type: DELETE_TASK,
    payload: id
})

export const switchTask = (id: number): TaskAction => ({
    type: SWITCH_TASK,
    payload: id,
})

export const updateTask = (id: number): TaskAction => ({
    type: UPDATE_TASK,
    payload: id
})