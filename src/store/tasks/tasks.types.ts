export const CREATE_TASK = "CREATE_TASK"
export const DELETE_TASK = "DELETE_TASK"
export const SWITCH_TASK = "SWITCH_TASK"
export const UPDATE_TASK = "UPDATE_TASK"

export interface ITask {
    readonly id: number
    text: string
    timestamp: number
    isPaused: boolean
}

export interface TasksState {
    tasksList: ITask[]
}

interface CreateTaskAction {
    type: typeof CREATE_TASK
    payload: string
}

interface DeleteTaskAction {
    type: typeof DELETE_TASK
    payload: number
}

interface UpdateTaskAction {
    type: typeof UPDATE_TASK
    payload: number
}

interface SwitchTaskAction {
    type: typeof SWITCH_TASK
    payload: number
}

export type TaskAction = CreateTaskAction | DeleteTaskAction | SwitchTaskAction | UpdateTaskAction