import {ITask, TaskAction, TasksState} from "./tasks.types";

const initialState: TasksState = {
    tasksList: []
}

export const tasksReducer = (state = initialState, action: TaskAction): TasksState => {
    switch (action.type) {
        case "CREATE_TASK":
            const newTask: ITask = {
                id: Date.now(),
                text: action.payload,
                timestamp: 0,
                isPaused: false
            }
            return {...state, tasksList: [...state.tasksList, newTask]}
        case "DELETE_TASK":
            return {...state, tasksList: state.tasksList.filter(item => item.id !== action.payload)}
        case "UPDATE_TASK":
            console.log(123)
            return {
                ...state,
                tasksList: state.tasksList.map((item, id) => item.id === action.payload ? {
                    ...state.tasksList[id],
                    timestamp: state.tasksList[id].timestamp + 1000
                } : item)
            }
        case "SWITCH_TASK":
            return {
                ...state,
                tasksList: state.tasksList.map((item, id) => item.id === action.payload ? {
                    ...state.tasksList[id],
                    isPaused: !state.tasksList[id].isPaused
                } : item)
            }
        default:
            return state
    }
}