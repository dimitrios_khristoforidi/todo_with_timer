import * as path from 'path';
import * as webpack from 'webpack';
import * as HtmlWebPackPlugin from "html-webpack-plugin";

const config: webpack.Configuration = {
    mode: 'development',
    entry: './src/index.tsx',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 3000,
        headers: {
            "Content-Security-Policy": "default-src 'self' data: ; script-src 'self' 'unsafe-inline' 'unsafe-eval'; style-src 'self' https://fonts.googleapis.com 'unsafe-inline'; base-uri 'self'; font-src 'self' https://fonts.gstatic.com https://fonts.googleapis.com data:; img-src 'self' data:"
        },
        historyApiFallback: true
    },
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                exclude: /node_modules/,
                use: {loader: 'ts-loader',},
            },
            {
                test: /\.css$/,
                use: [
                    {loader: 'style-loader',},
                    {loader: 'css-loader',},
                ],
            },
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: './src/index.html',
        }),
    ],
};

export default config;